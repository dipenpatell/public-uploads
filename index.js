document.addEventListener("DOMContentLoaded", function() {
      // Close dropdown if clicked outside
      document.addEventListener("click", function(event) {
        var dropdown = document.querySelector(".dropdown");
        if (!dropdown.contains(event.target)) {
          hideOptions();
        }
      });


      const machineDetails = document.querySelector(".machine-details ul"),
        machineName = machineDetails.querySelector(".machine-name"),
        machineStatus = machineDetails.querySelector(".machine-status"),
        machineMode = machineDetails.querySelector(".machine-mode"),
        program = machineDetails.querySelector(".program"),
        operator = machineDetails.querySelector(".operator"),
        downTime = machineDetails.querySelector(".downtime"),
        cycletime = machineDetails.querySelector(".cycletime"),
        tool = machineDetails.querySelector(".tool"),
        healthy = machineDetails.querySelector(".healthy");

      machineName.querySelector(".count p").textContent = machines[0].machineName;
      machineStatus.querySelector(".count p").textContent = (machines[0].machineStatus) ? "ACTIVE" : "ON HOLD";
      machineMode.querySelector(".count p").textContent = machines[0].machineMode;
      program.querySelector(".count p").textContent = machines[0].program;
      operator.querySelector(".count p").textContent = machines[0].operator;
      downTime.querySelector(".count p").textContent = machines[0].downTime;
      cycletime.querySelector(".count p").textContent = machines[0].cycleTime;
      tool.querySelector(".count p").textContent = machines[0].tool;
      healthy.querySelector(".count p").textContent = machines[0].healthy;
      // Toggle options on select click
      document.querySelector(".select").addEventListener("click", function() {
        var dropdown = this.closest(".dropdown");
        var options = dropdown.querySelector(".option-list");
        var searchBox = dropdown.querySelector(".search-box");

        toggleElement(options);
        toggleElement(searchBox);
        // Handle option click
        options.addEventListener("click", function(event) {
          var selectedMachine = machines.find(function(machine) {
            return machine.machineName === event.target.textContent;
          });
          if (event.target.tagName === "LI") {
            var selectedOption = event.target.textContent;
            dropdown.querySelector(".select").textContent = selectedOption;
            machineName.querySelector(".count p").textContent = selectedMachine.machineName;
            machineStatus.querySelector(".count p").textContent = (selectedMachine.machineStatus) ? "ACTIVE" : "ON HOLD";
            machineMode.querySelector(".count p").textContent = selectedMachine.machineMode;
            program.querySelector(".count p").textContent = selectedMachine.program;
            operator.querySelector(".count p").textContent = selectedMachine.operator;
            downTime.querySelector(".count p").textContent = selectedMachine.downTime;
            cycletime.querySelector(".count p").textContent = selectedMachine.cycleTime;
            tool.querySelector(".count p").textContent = selectedMachine.tool;
            healthy.querySelector(".count p").textContent = selectedMachine.healthy;
            hideOptions();
          }
        });
      });
      // Handle search
      document.querySelector(".search-control").addEventListener("input", function() {
        var searchTerm = this.value.toLowerCase();
        var listItems = document.querySelectorAll(".dropdown .option-list li");

        listItems.forEach(function(item) {
          var text = item.textContent.toLowerCase();
          item.style.display = text.includes(searchTerm) ? "block" : "none";
        });
      });

      // Function to hide options and search box
      function hideOptions() {
        var dropdown = document.querySelector(".dropdown");
        dropdown.querySelector(".option-list").style.display = "none";
        dropdown.querySelector(".search-box").style.display = "none";
      }

      // Function to toggle an element's display
      function toggleElement(element) {
        element.style.display = element.style.display === "none" ? "block" : "none";
      }
    });

    function updateTable() {
      var pinnedTableBody = document.querySelector('#pinnedTable');
        var unpinnedTableBody = document.querySelector('#unpinnedTable');
      
        var pinnedRows = [];
        machines.forEach(function (machine, i) {
          var n = i + 1;
          
          // Create a new table row
          var row = document.createElement('tr');
          row.id = n;
          var machinename = (machine.machineName.length > 16) ? machine.machineName.substring(0, 16) + "..." : machine.machineName;
          
          // Add onclick event to toggle pin/unpin
          row.onclick = function () {
                if (pinnedRows.includes(row)) {
                    // Unpin the row
                    const index = pinnedRows.indexOf(row);
                    pinnedRows.splice(index, 1);
                    // Move the row to the unpinned table
                    unpinnedTableBody.appendChild(row);
                  } else {
                    // Remove the row from its current position
                    row.parentNode.removeChild(row);
                    // Insert the row at the beginning of the table
                    pinnedTableBody.insertBefore(row, pinnedTableBody.firstChild);
                    // Add the row to the pinnedRows array
                    pinnedRows.unshift(row);
                  }
                  for (var i = 0; i < pinnedTableBody.rows.length; i++) {
                    pinnedTableBody.rows[i].querySelector(".machinename svg").style.display = "block";
                    if(i % 2 == 0){
                      pinnedTableBody.rows[i].style.backgroundColor = "#242424";
                    }
                    else{
                      pinnedTableBody.rows[i].style.backgroundColor = "#2c2c2c"
                    }
                  }
                  let odd = (pinnedTableBody.rows.length % 2 == 0) ? false : true;
                  for (let i = 0; i < unpinnedTableBody.rows.length; i++) {
                    unpinnedTableBody.rows[i].querySelector(".machinename svg").style.display = "none";
                    if(i % 2 == 0 && odd == false){
                      unpinnedTableBody.rows[i].style.backgroundColor = "#242424";
                      }
                      else if(i % 2 != 0 && odd == false ){
                        unpinnedTableBody.rows[i].style.backgroundColor = "#2c2c2c";
                      }else if(i % 2 == 0 && odd  == true){
                        unpinnedTableBody.rows[i].style.backgroundColor = "#2c2c2c";
                      }
                      else if(i % 2 != 0 && odd  == true){
                        unpinnedTableBody.rows[i].style.backgroundColor = "#242424";
                      }
                  }
            };
            // Populate the row with data
            row.innerHTML = `
            <td>
                <div class="machinename" style="height: 100%; width: 100%;">
                  <svg height="15px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M19.1835 7.80516L16.2188 4.83755C14.1921 2.8089 13.1788 1.79457 12.0904 2.03468C11.0021 2.2748 10.5086 3.62155 9.5217 6.31506L8.85373 8.1381C8.59063 8.85617 8.45908 9.2152 8.22239 9.49292C8.11619 9.61754 7.99536 9.72887 7.86251 9.82451C7.56644 10.0377 7.19811 10.1392 6.46145 10.3423C4.80107 10.8 3.97088 11.0289 3.65804 11.5721C3.5228 11.8069 3.45242 12.0735 3.45413 12.3446C3.45809 12.9715 4.06698 13.581 5.28476 14.8L6.69935 16.2163L2.22345 20.6964C1.92552 20.9946 1.92552 21.4782 2.22345 21.7764C2.52138 22.0746 3.00443 22.0746 3.30236 21.7764L7.77841 17.2961L9.24441 18.7635C10.4699 19.9902 11.0827 20.6036 11.7134 20.6045C11.9792 20.6049 12.2404 20.5358 12.4713 20.4041C13.0192 20.0914 13.2493 19.2551 13.7095 17.5825C13.9119 16.8472 14.013 16.4795 14.2254 16.1835C14.3184 16.054 14.4262 15.9358 14.5468 15.8314C14.8221 15.593 15.1788 15.459 15.8922 15.191L17.7362 14.4981C20.4 13.4973 21.7319 12.9969 21.9667 11.9115C22.2014 10.826 21.1954 9.81905 19.1835 7.80516Z" fill="white"/>
</svg>
                    <div class="progress progress-striped active">
                      <span class="sr-only">${machinename}</span>
                        <div class="progress-bar" id="progress-bar-${n}" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%;   background: linear-gradient(to right, #689121 ${machine.progress}%, transparent 0%);">
                          </div>
                          </div>
                          </div>
            </td>
            <td class="act">
                <div>
                    <figure class="highcharts-figure">
                        <div id="ACT-TRG-${n}"></div>
                        </figure>
                        <a><span>${machine.act}</span> / <span>${machine.trg}</span></a>
                        </div>
                        </td>
                        <td>
                <figure class="highcharts-figure">
                  <div id="OEE-${n}"></div>
                </figure>
            </td>
            <td class="operator"><span>${machine.operator}</span><span class="tooltiptext tooltip-right">${machine.operator}</span></td>
            `;
            if (pinnedRows.includes(row)) {
                pinnedTableBody.appendChild(row);
            } else {
                unpinnedTableBody.appendChild(row);
            }
        // Append the row to the table body
        // tableBody.appendChild(row);
      });
    }

    document.getElementById("searchInput").addEventListener("input", function() {
      // Get the search value
      var searchText = this.value.toLowerCase();

      // Get all table rows
      var rows = document.getElementById("unpinnedTable").getElementsByTagName("tr");
      
      // Loop through the rows and hide/show based on the search value
      for (var i = 0; i < rows.length; i++) {
        var machineName = rows[i].textContent.toLowerCase();
        rows[i].style.display = machineName.indexOf(searchText) > -1 ? "" : "none";
      }
    });
    // Function to update the UL with the fetched data
    function updateUI() {
      var machineList = document.querySelector('.option-list');
      // Add new list items based on the fetched data
      machines.forEach(function(machine, index) {
        var listItem = document.createElement('li');
        listItem.id = 'machine_' + index; // Assign a unique ID
        listItem.textContent = machine.machineName;
        machineList.appendChild(listItem);
      });
    }

    function changeShift(evt, clickedbtn) {
      var thisshift = document.getElementsByClassName("this-shift");
      var prevShift = document.getElementsByClassName("prev-shift");
      var today = document.getElementsByClassName("today");
      for (i = 0; i < thisshift.length; i++) {
        thisshift[i].className = thisshift[i].className.replace(" primary", "");
      }
      for (i = 0; i < prevShift.length; i++) {
        prevShift[i].className = prevShift[i].className.replace(" primary", "");
      }
      for (i = 0; i < today.length; i++) {
        today[i].className = today[i].className.replace(" primary", "");
      }
      evt.currentTarget.className += " primary";
    }
    // async function fetchData() {
    //       try {
    //                 // Using the fetch API, which returns a Promise
    //                 const response = await fetch('/api/machines');

    //                 if (!response.ok) {
    //                     throw new Error('Failed to fetch data');
    //                 }

    //                 // Parse the JSON response
    //                 machines = await response.json();
    var machines = [{
        "machineName": "CNC-01",
        "act": 700,
        "trg": 1000,
        "productionTime": "10:14 hr",
        "Availability": 70,
        "Performance": 90,
        "Quality": 80,
        "machineStatus": false,
        "machineMode": "MDA",
        "program": "ABCZYX1234578",
        "operator": "Suresh",
        "downTime": "Break Time (4mins)",
        "cycleTime": "400/450 (program)",
        "tool": "Tool3",
        "progress": 70,
        "healthy": "OK"
      },
      {
        "machineName": "CNC-02 (DX-100)",
        "act": 800,
        "trg": 1000,
        "productionTime": "10:14 hr",
        "Availability": 80,
        "Performance": 85,
        "Quality": 70,
        "machineStatus": true,
        "machineMode": "MDA",
        "program": "ABCZYX12214578",
        "operator": "Ramesh",
        "downTime": "Break Time (5mins)",
        "cycleTime": "450/500 (program)",
        "tool": "Tool1",
        "progress": 75,
        "healthy": "EXCELLENT"
      },
      {
        "machineName": "CNC-03 (DX-100)",
        "act": 800,
        "trg": 1000,
        "productionTime": "10:14 hr",
        "Availability": 90,
        "Performance": 80,
        "Quality": 70,
        "machineStatus": true,
        "machineMode": "MDA",
        "program": "ABCZYX12214578",
        "operator": "Ajay",
        "downTime": "Break Time (5mins)",
        "cycleTime": "450/500 (program)",
        "tool": "Tool2",
        "progress": 50,
        "healthy": "EXCELLENT"
      },
      {
        "machineName": "CNC-04 (LINEAR-8020)",
        "act": 700,
        "trg": 1000,
        "productionTime": "10:14 hr",
        "Availability": 80,
        "Performance": 70,
        "Quality": 90,
        "machineStatus": false,
        "machineMode": "MDA",
        "program": "ABCZYX1234578",
        "operator": "Suresh",
        "downTime": "Break Time (4mins)",
        "cycleTime": "400/450 (program)",
        "tool": "Tool3",
        "progress": 100,
        "healthy": "OK"
      },
      {
        "machineName": "CNC-05 (DX-60)",
        "act": 700,
        "trg": 1000,
        "productionTime": "10:14 hr",
        "Availability": 70,
        "Performance": 90,
        "Quality": 80,
        "machineStatus": true,
        "machineMode": "MDA",
        "program": "ABCZYX12214578",
        "operator": "Ramesh",
        "downTime": "Break Time (5mins)",
        "cycleTime": "450/500 (program)",
        "tool": "Tool1",
        "progress": 50,
        "healthy": "EXCELLENT"
      },
      {
        "machineName": "CNC-06 (DX-60)",
        "act": 600,
        "trg": 1000,
        "productionTime": "10:14 hr",
        "Availability": 80,
        "Performance": 80,
        "Quality": 70,
        "machineStatus": true,
        "machineMode": "MDA",
        "program": "ABCZYX12214578",
        "operator": "Ajay",
        "downTime": "Break Time (5mins)",
        "cycleTime": "450/500 (program)",
        "tool": "Tool2",
        "progress": 40,
        "healthy": "EXCELLENT"
      },
      {
        "machineName": "CNC-07 (LINEAR-8020)",
        "act": 800,
        "trg": 1000,
        "productionTime": "10:14 hr",
        "Availability": 90,
        "Performance": 70,
        "Quality": 90,
        "machineStatus": false,
        "machineMode": "MDA",
        "program": "ABCZYX1234578",
        "operator": "Suresh",
        "downTime": "Break Time (4mins)",
        "cycleTime": "400/450 (program)",
        "tool": "Tool3",
        "progress": 10,
        "healthy": "OK"
      },
      {
        "machineName": "CNC-08 (LINEAR-8020)",
        "act": 900,
        "trg": 1000,
        "productionTime": "10:14 hr",
        "Availability": 70,
        "Performance": 70,
        "Quality": 80,
        "machineStatus": true,
        "machineMode": "MDA",
        "program": "ABCZYX12214578",
        "operator": "Ramesh",
        "downTime": "Break Time (5mins)",
        "cycleTime": "450/500 (program)",
        "tool": "Tool1",
        "progress": 75,
        "healthy": "EXCELLENT"
      },
      {
        "machineName": "CNC-09 (LINEAR-8020)",
        "act": 800,
        "trg": 850,
        "productionTime": "10:14 hr",
        "Availability": 90,
        "Performance": 90,
        "Quality": 70,
        "machineStatus": true,
        "machineMode": "MDA",
        "program": "ABCZYX12214578",
        "operator": "Ajay",
        "downTime": "Break Time (5mins)",
        "cycleTime": "450/500 (program)",
        "tool": "Tool2",
        "progress": 90,
        "healthy": "EXCELLENT"
      },
      {
        "machineName": "CNC-04 (LINEAR-8020)",
        "act": 900,
        "trg": 1000,
        "productionTime": "10:14 hr",
        "Availability": 70,
        "Performance": 70,
        "Quality": 90,
        "machineStatus": false,
        "machineMode": "MDA",
        "program": "ABCZYX1234578",
        "operator": "Suresh",
        "downTime": "Break Time (4mins)",
        "cycleTime": "400/450 (program)",
        "tool": "Tool3",
        "progress": 100,
        "healthy": "OK"
      },
      {
        "machineName": "CNC-10 (DX-100)",
        "act": 700,
        "trg": 1000,
        "productionTime": "10:14 hr",
        "Availability": 80,
        "Performance": 80,
        "Quality": 80,
        "machineStatus": false,
        "machineMode": "MDA",
        "program": "ABCZYX12214578",
        "operator": "Ramesh",
        "downTime": "Break Time (5mins)",
        "cycleTime": "450/500 (program)",
        "tool": "Tool1",
        "progress": 90,
        "healthy": "EXCELLENT"
      },
      {
        "machineName": "CNC-11 (DX-100)",
        "act": 900,
        "trg": 1000,
        "productionTime": "10:14 hr",
        "Availability": 90,
        "Performance": 70,
        "Quality": 70,
        "machineStatus": true,
        "machineMode": "MDA",
        "program": "ABCZYX12214578",
        "operator": "Ajay",
        "downTime": "Break Time (5mins)",
        "cycleTime": "450/500 (program)",
        "tool": "Tool2",
        "progress": 80,
        "healthy": "EXCELLENT"
      }
    ];

    updateTable();
    updateUI(); // Call a function to update the UI with the new data
    {
      //SVG gauges script
      var firstGauge = document.querySelector(
        ".roundGauge:first-of-type .progress"
      );
      var firstTarget = parseInt(firstGauge.getAttribute("data-target"));
      var firstGaugeReadout = document.querySelector(
        ".roundGauge .gauge-container .center .value"
      );

      //variables
      var gaugeR = parseInt(
        document.querySelectorAll("circle")[0].getAttribute("r")
      );
      var gaugeC = gaugeR * Math.PI * 2;
      var animationDuration = 1.5;

      //init svg circles
      var circles = document.querySelectorAll("circle");
      var gauges = document.querySelectorAll(".progress");
      TweenMax.set(circles, {
        strokeDashoffset: gaugeC,
      });

      TweenMax.set(gauges, {
        attr: {
          "stroke-dasharray": gaugeC + " " + gaugeC,
        },
      });

      //calculate the offset
      function calculateOffset(t, c) {
        var target = c - (c * t) / 100;
        return target;
      }

      //timeline
      var tl = new TimelineMax();

      //first gauge animation
      tl.to(firstGauge, animationDuration, {
        strokeDashoffset: calculateOffset(firstTarget, gaugeC),
        ease: Bounce.easeOut,
        onUpdate: function() {
          var currentStrokeOffset = parseInt(firstGauge.style.strokeDashoffset);
          firstGaugeReadout.textContent = Math.round(
            100 - (currentStrokeOffset * 100) / gaugeC
          );
        },
      });
    }

    //Production-Count
    Highcharts.chart("Production-Count-linechart", {
      chart: {
        height: "50",
        width: "126",
        backgroundColor: "none",
        margin: [0, 0, 0, 0],
      },
      title: {
        text: "",
      },
      xAxis: {
        type: "datetime",
        lineWidth: 0,
        minorGridLineWidth: 0,
        lineColor: "transparent",
        labels: {
          enabled: false,
        },
        minorTickLength: 0,
        tickLength: 0,
      },
      yAxis: {
        title: false,
        lineWidth: 0,
        minorGridLineWidth: 0,
        lineColor: "transparent",
        labels: {
          enabled: true,
        },
        minorTickLength: 0,
        tickLength: 0,
        gridLineWidth: 0,
        labels: {
          style: {
            color: "white",
          },
        },
      },
      legend: {
        enabled: false,
      },
      plotOptions: {
        area: {
          fillColor: {
            linearGradient: {
              x1: 0,
              y1: 0,
              x2: 0,
              y2: 1,
            },
            stops: [
              [0, "#548646"],
              [1, Highcharts.color("#548646").setOpacity(0).get("rgba")],
            ],
          },
          marker: {
            radius: 2,
          },
          lineWidth: 1,
          states: {
            hover: {
              lineWidth: 1,
            },
          },
          threshold: null,
        },
        series: {
          color: "#548646",
        },
      },

      series: [{
        type: "area",
        name: "count",
        data: [
          [1],
          [2],
          [1.5],
          [0.697],
          [0.6992],
          [0.7007]
        ],
      }, ],
    });

    //Productive-Time
    {
      //SVG gauges script
      var secGauge = document.querySelector(
        ".roundGauge-2:first-of-type .progress"
      );
      var firstTarget = parseInt(secGauge.getAttribute("data-target"));
      var secReadout = document.querySelector(
        ".roundGauge-2 .gauge-container .center .value"
      );

      //variables
      var gaugeR = parseInt(
        document.querySelectorAll("circle")[0].getAttribute("r")
      );
      var gaugeC = gaugeR * Math.PI * 2;
      var animationDuration = 1.5;

      //init svg circles
      var circles = document.querySelectorAll("circle");
      var gauges = document.querySelectorAll(".progress");
      TweenMax.set(circles, {
        strokeDashoffset: gaugeC,
      });

      TweenMax.set(gauges, {
        attr: {
          "stroke-dasharray": gaugeC + " " + gaugeC,
        },
      });

      //calculate the offset
      function calculateOffset(t, c) {
        var target = c - (c * t) / 100;
        return target;
      }

      //timeline
      var tl = new TimelineMax();

      //first gauge animation
      tl.to(secGauge, animationDuration, {
        strokeDashoffset: calculateOffset(firstTarget, gaugeC),
        ease: Bounce.easeOut,
        onUpdate: function() {
          var currentStrokeOffset = parseInt(secGauge.style.strokeDashoffset);
          secReadout.textContent = Math.round(
            100 - (currentStrokeOffset * 100) / gaugeC
          );
        },
      });
    }

    //Productive Time
    Highcharts.chart("Productive-Time-linechart", {
      chart: {
        height: "50",
        width: "126",
        backgroundColor: "none",
        margin: [0, 0, 0, 0],
      },
      title: {
        text: "",
      },
      xAxis: {
        type: "datetime",
        lineWidth: 0,
        minorGridLineWidth: 0,
        lineColor: "transparent",
        labels: {
          enabled: false,
        },
        minorTickLength: 0,
        tickLength: 0,
      },
      yAxis: {
        title: false,
        lineWidth: 0,
        minorGridLineWidth: 0,
        lineColor: "transparent",
        labels: {
          enabled: true,
        },
        minorTickLength: 0,
        tickLength: 0,
        gridLineWidth: 0,
        labels: {
          style: {
            color: "white",
          },
        },
      },
      legend: {
        enabled: false,
      },
      plotOptions: {
        area: {
          fillColor: {
            linearGradient: {
              x1: 0,
              y1: 0,
              x2: 0,
              y2: 1,
            },
            stops: [
              [0, "#24629e"],
              [1, Highcharts.color("#24629e").setOpacity(0).get("rgba")],
            ],
          },
          marker: {
            radius: 2,
          },
          lineWidth: 1,
          states: {
            hover: {
              lineWidth: 1,
            },
          },
          threshold: null,
        },
        series: {
          color: "#24629e",
        },
      },

      series: [{
        type: "area",
        name: "count",
        data: [
          [0.7537],
          [0.6951],
          [0.6925],
          [0.697],
          [0.6992],
          [0.7007]
        ],
      }, ],
    });

    //Availability
    Highcharts.chart("Availability-gauge", {
      chart: {
        type: "gauge",
        backgroundColor: "none",
        plotBackgroundColor: null,
        plotBackgroundImage: null,
        plotBorderWidth: 0,
        plotShadow: false,
        height: "83",
        width: "90",
        margin: [0, 0, 0, 0],
      },

      title: {
        text: "",
      },

      pane: {
        startAngle: -90,
        endAngle: 89.9,
        background: null,
        center: ["50%", "75%"],
        size: "100%",
      },

      // the value axis
      yAxis: {
        min: 0,
        max: 200,
        tickPixelInterval: 72,
        tickPosition: "inside",
        tickColor: Highcharts.defaultOptions.chart.backgroundColor || "#FFFFFF",
        tickLength: 20,
        tickWidth: 2,
        minorTickInterval: null,
        labels: {
          enabled: false,
        },
        lineWidth: 0,
        plotBands: [{
            from: 0,
            to: 65,
            color: "#DF5353", // red
            thickness: 15,
          },
          {
            from: 65,
            to: 130,
            color: "#DDDF0D", // yellow
            thickness: 15,
          },
          {
            from: 130,
            to: 200,
            color: "#55BF3B", // green
            thickness: 15,
          },
        ],
      },

      series: [{
        name: "Speed",
        data: [80],
        tooltip: {
          valueSuffix: "%",
        },
        dataLabels: {
          enabled: false,
          format: "{y} %",
          borderWidth: 0,
          color: "#548646",
          margin: [10, 0, 0, 0],
          style: {
            fontSize: "10px",
            textOutline: "none",
          },
        },
        dial: {
          radius: "80%",
          backgroundColor: "gray",
          baseWidth: 5,
          baseLength: "0%",
          rearLength: "0%",
        },
        pivot: {
          backgroundColor: "gray",
          radius: 3,
        },
      }, ],
    });

    // Add some life
    setInterval(() => {
      const chart = Highcharts.charts[0];
      if (chart && !chart.renderer.forExport) {
        const point = chart.series[0].points[0],
          inc = Math.round((Math.random() - 0.5) * 20);

        let newVal = point.y + inc;
        if (newVal < 0 || newVal > 200) {
          newVal = point.y - inc;
        }

        point.update(newVal);
      }
    }, 3000);

    //Performance
    Highcharts.chart("Performance-gauge", {
      chart: {
        type: "gauge",
        backgroundColor: "none",
        plotBackgroundColor: null,
        plotBackgroundImage: null,
        plotBorderWidth: 0,
        plotShadow: false,
        height: "83",
        width: "90",
        margin: [0, 0, 0, 0],
      },

      title: {
        text: "",
      },

      pane: {
        startAngle: -90,
        endAngle: 89.9,
        background: null,
        center: ["50%", "75%"],
        size: "100%",
      },

      // the value axis
      yAxis: {
        min: 0,
        max: 200,
        tickPixelInterval: 72,
        tickPosition: "inside",
        tickColor: Highcharts.defaultOptions.chart.backgroundColor || "#FFFFFF",
        tickLength: 20,
        tickWidth: 2,
        minorTickInterval: null,
        labels: {
          distance: 20,
          style: {
            fontSize: "10px",
          },
        },
        lineWidth: 0,
        plotBands: [{
            from: 0,
            to: 65,
            color: "#DF5353", // red
            thickness: 15,
          },
          {
            from: 65,
            to: 130,
            color: "#DDDF0D", // yellow
            thickness: 15,
          },
          {
            from: 130,
            to: 200,
            color: "#55BF3B", // green
            thickness: 15,
          },
        ],
      },

      series: [{
        name: "Speed",
        data: [80],
        tooltip: {
          valueSuffix: "%",
        },
        dataLabels: {
          enabled: false,
          format: "{y} %",
          borderWidth: 0,
          color: "#548646",
          style: {
            fontSize: "12px",
            textOutline: "none",
          },
        },
        dial: {
          radius: "80%",
          backgroundColor: "gray",
          baseWidth: 8,
          baseLength: "0%",
          rearLength: "0%",
        },
        pivot: {
          backgroundColor: "gray",
          radius: 4,
        },
      }, ],
    });

    // Add some life
    setInterval(() => {
      const chart = Highcharts.charts[0];
      if (chart && !chart.renderer.forExport) {
        const point = chart.series[0].points[0],
          inc = Math.round((Math.random() - 0.5) * 20);

        let newVal = point.y + inc;
        if (newVal < 0 || newVal > 200) {
          newVal = point.y - inc;
        }

        point.update(newVal);
      }
    }, 3000);

    //OEE
    Highcharts.chart("OEE-gauge", {
      chart: {
        type: "gauge",
        backgroundColor: "none",
        plotBackgroundColor: null,
        plotBackgroundImage: null,
        plotBorderWidth: 0,
        plotShadow: false,
        height: "83",
        width: "90",
        margin: [0, 0, 0, 0],
      },

      title: {
        text: "",
      },

      pane: {
        startAngle: -90,
        endAngle: 89.9,
        background: null,
        center: ["50%", "75%"],
        size: "100%",
      },

      // the value axis
      yAxis: {
        min: 0,
        max: 200,
        tickPixelInterval: 72,
        tickPosition: "inside",
        tickColor: Highcharts.defaultOptions.chart.backgroundColor || "#FFFFFF",
        tickLength: 20,
        tickWidth: 2,
        minorTickInterval: null,
        labels: {
          distance: 20,
          style: {
            fontSize: "10px",
          },
        },
        lineWidth: 0,
        plotBands: [{
            from: 0,
            to: 65,
            color: "#DF5353", // red
            thickness: 15,
          },
          {
            from: 65,
            to: 130,
            color: "#DDDF0D", // yellow
            thickness: 15,
          },
          {
            from: 130,
            to: 200,
            color: "#55BF3B", // green
            thickness: 15,
          },
        ],
      },

      series: [{
        name: "Speed",
        data: [80],
        tooltip: {
          valueSuffix: "%",
        },
        dataLabels: {
          enabled: false,
          format: "{y} %",
          borderWidth: 0,
          color: "#548646",
          style: {
            fontSize: "12px",
            textOutline: "none",
          },
        },
        dial: {
          radius: "80%",
          backgroundColor: "gray",
          baseWidth: 8,
          baseLength: "0%",
          rearLength: "0%",
        },
        pivot: {
          backgroundColor: "gray",
          radius: 4,
        },
      }, ],
    });

    // Add some life
    setInterval(() => {
      const chart = Highcharts.charts[0];
      if (chart && !chart.renderer.forExport) {
        const point = chart.series[0].points[0],
          inc = Math.round((Math.random() - 0.5) * 20);

        let newVal = point.y + inc;
        if (newVal < 0 || newVal > 200) {
          newVal = point.y - inc;
        }

        point.update(newVal);
      }
    }, 3000);

    // //Shift1
    var ct1shift1 = 80;
    var ct1shift2 = 50;
    var ct1total = 69;
    Highcharts.chart("shift1-solidgauge", {
      chart: {
        type: "solidgauge",
        width: "80",
        height: "70",
        backgroundColor: "none",
        margin: [0, 0, 0, 0],
      },

      title: {
        text: "",
        style: {
          fontSize: "24px",
        },
      },
      tooltip: {
        borderWidth: 0,
        backgroundColor: "none",
        shadow: false,
        style: {
          fontSize: "16px",
        },
        pointFormat: '{series.name}<br><span style="font-size:2em; color: {point.color}; font-weight: bold">{point.y}%</span>',
        positioner: function(labelWidth, labelHeight) {
          return {
            x: 200 - labelWidth / 2,
            y: 180,
          };
        },
      },

      pane: {
        startAngle: 0,
        endAngle: 360,
        background: [{
            // Track for Conversion
            outerRadius: "112%",
            innerRadius: "88%",
            backgroundColor: "#565656",
            borderWidth: 0,
          },
          {
            // Track for Engagement
            outerRadius: "87%",
            innerRadius: "63%",
            backgroundColor: "#565656",
            borderWidth: 0,
          },
          {
            // Track for Feedback
            outerRadius: "62%",
            innerRadius: "38%",
            backgroundColor: "#565656",
            borderWidth: 0,
          },
        ],
      },

      yAxis: {
        min: 0,
        max: 100,
        lineWidth: 0,
        tickPositions: [],
      },

      plotOptions: {
        solidgauge: {
          dataLabels: {
            enabled: false,
          },
          linecap: "round",
          stickyTracking: false,
          rounded: true,
        },
      },
      legend: {
        labelFormatter: function() {
          return (
            '<span style="text-weight:bold;color:' +
            this.userOptions.color +
            '">' +
            this.name +
            "</span>"
          );
        },
        symbolWidth: 0,
      },
      series: [{
          name: "Shift 1",
          data: [{
            color: "#24629E",
            radius: "112%",
            innerRadius: "88%",
            y: ct1shift1,
          }, ],
          legend: {
            enabled: true,
          },
        },
        {
          name: "Shift 2",
          data: [{
            color: "#DABB2F",
            radius: "87%",
            innerRadius: "63%",
            y: ct1shift2,
          }, ],
          legend: {
            enabled: true,
          },
        },
        {
          name: "total",
          data: [{
            color: "#548646",
            radius: "62%",
            innerRadius: "38%",
            y: ct1total,
          }, ],
          legend: {
            enabled: true,
          },
        },
      ],
    });

    document.querySelector(".ct-1 #s1-count").textContent = ct1shift1 + "%";
    document.querySelector(".ct-1 #s2-count").textContent = ct1shift2 + "%";
    document.querySelector(".ct-1 #total-count").textContent = ct1total + "%";

    var ct2shift1 = 80;
    var ct2shift2 = 80;
    var ct2total = 80;
    //Shift2
    Highcharts.chart("shift2-solidgauge", {
      chart: {
        type: "solidgauge",
        width: "70",
        height: "80",
        backgroundColor: "none",
        margin: [0, 0, 0, 0],
      },

      title: {
        text: "",
        style: {
          fontSize: "24px",
        },
      },
      tooltip: {
        borderWidth: 0,
        backgroundColor: "none",
        shadow: false,
        style: {
          fontSize: "16px",
        },
        valueSuffix: "%",
        pointFormat: "{series.name}<br>" +
          '<span style="font-size: 2em; color: {point.color}; ' +
          'font-weight: bold">{point.y}</span>',
        positioner: function(labelWidth) {
          return {
            x: (this.chart.chartWidth - labelWidth) / 2,
            y: this.chart.plotHeight / 2 + 15,
          };
        },
      },

      pane: {
        startAngle: 0,
        endAngle: 360,
        background: [{
            // Track for Conversion
            outerRadius: "112%",
            innerRadius: "88%",
            backgroundColor: "#565656",
            borderWidth: 0,
          },
          {
            // Track for Engagement
            outerRadius: "87%",
            innerRadius: "63%",
            backgroundColor: "#565656",
            borderWidth: 0,
          },
          {
            // Track for Feedback
            outerRadius: "62%",
            innerRadius: "38%",
            backgroundColor: "#565656",
            borderWidth: 0,
          },
        ],
      },

      yAxis: {
        min: 0,
        max: 100,
        lineWidth: 0,
        tickPositions: [],
      },

      plotOptions: {
        solidgauge: {
          dataLabels: {
            enabled: false,
          },
          linecap: "round",
          stickyTracking: false,
          rounded: true,
        },
      },

      series: [{
          name: "Conversion",
          data: [{
            color: "#24629E",
            radius: "112%",
            innerRadius: "88%",
            y: ct2shift1,
          }, ],
          legend: {
            enabled: true,
          },
        },
        {
          name: "Engagement",
          data: [{
            color: "#DABB2F",
            radius: "87%",
            innerRadius: "63%",
            y: ct2shift2,
          }, ],
          legend: {
            enabled: true,
          },
        },
        {
          name: "Feedback",
          data: [{
            color: "#548646",
            radius: "62%",
            innerRadius: "38%",
            y: ct2total,
          }, ],
          legend: {
            enabled: true,
          },
        },
      ],
    });
    document.querySelector(".ct-2 #s1-count").textContent = ct2shift1 + "%";
    document.querySelector(".ct-2 #s2-count").textContent = ct2shift2 + "%";
    document.querySelector(".ct-2 #total-count").textContent = ct2total + "%";

    //Time Analysis
    Highcharts.chart("Time-Analysis", {
      chart: {
        type: "column",
        // width: "410",
        height: "160",
        backgroundColor: "none",
      },
      title: {
        text: "",
      },
      xAxis: {
        categories: [
          "12 AM",
          "1 AM",
          "2 AM",
          "3 AM",
          "4 AM",
          "5 AM",
          "6 AM",
          "7 AM",
          "8 AM",
          "9 AM",
          "10 AM",
          "11 AM",
          "12 PM",
          "01 PM",
          "02 PM",
          "03 PM",
          "4 PM",
          "4 PM",
          "5 PM",
          "6 PM",
          "7 PM",
          "8 PM",
          "9 PM",
          "10 PM",
          "11 PM",
        ],
        crosshair: true,
        accessibility: {
          description: "Countries",
        },
        lineWidth: 0,
        minorGridLineWidth: 0,
        lineColor: "transparent",
        labels: {
          enabled: true,
        },
        minorTickLength: 0,
        tickLength: 0,
        gridLineWidth: 0,
        labels: {
          style: {
            fontSize: "12px",
            color: "white",
          },
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: "",
        },
        lineWidth: 0,
        minorGridLineWidth: 0,
        lineColor: "transparent",
        labels: {
          enabled: true,
        },
        minorTickLength: 0,
        tickLength: 0,
        gridLineWidth: 0,
        labels: {
          style: {
            color: "white",
          },
        },
      },
      legend: {
        align: "center",
        layout: "horizontal",
        verticalAlign: "bottom",
        x: 0,
        y: 24,
        floating: true,

        itemStyle: {
          color: "white",
          cursor: "pointer",
          fontSize: "12px",
          fontWeight: "bold",
        },
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      series: [{
          color: "#548646",
          name: "Productive time",
          data: [
            5, 25, 45, 30, 50, 70, 22, 30, 40, 35, 5, 25, 45, 30, 50, 70, 22, 30,
            40, 35, 5, 25, 45, 30,
          ],
        },
        {
          color: "#B32F3D",
          name: "Idle time",
          data: [
            40, 35, 5, 25, 45, 30, 70, 22, 30, 40, 35, 5, 25, 45, 30, 50, 5, 25, 45,
            30, 50, 70, 22, 30,
          ],
        },
      ],
    });

    //Production-Count
    Highcharts.chart("Production-Count", {
      chart: {
        type: "column",
        // width: "410",
        height: "180",
        backgroundColor: "none",
      },
      title: {
        text: "",
      },
      xAxis: {
        categories: [
          "12 AM",
          "1 AM",
          "2 AM",
          "3 AM",
          "4 AM",
          "5 AM",
          "6 AM",
          "7 AM",
          "8 AM",
          "9 AM",
          "10 AM",
          "11 AM",
          "12 PM",
          "01 PM",
          "02 PM",
          "03 PM",
          "4 PM",
          "4 PM",
          "5 PM",
          "6 PM",
          "7 PM",
          "8 PM",
          "9 PM",
          "10 PM",
          "11 PM",
        ],
        crosshair: true,
        accessibility: {
          description: "Countries",
        },
        lineWidth: 0,
        minorGridLineWidth: 0,
        lineColor: "transparent",
        labels: {
          enabled: true,
        },
        minorTickLength: 0,
        tickLength: 0,
        gridLineWidth: 0,
        labels: {
          style: {
            color: "white",
          },
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: "",
        },
        lineWidth: 0,
        minorGridLineWidth: 0,
        lineColor: "transparent",
        labels: {
          enabled: true,
        },
        minorTickLength: 0,
        tickLength: 0,
        gridLineWidth: 0,
        labels: {
          style: {
            color: "white",
          },
        },
      },
      legend: {
        align: "center",
        layout: "horizontal",
        verticalAlign: "bottom",
        x: 0,
        y: 24,
        floating: true,

        itemStyle: {
          color: "white",
          cursor: "pointer",
          fontSize: "12px",
          fontWeight: "bold",
        },
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      series: [{
          color: "#DABB2F",
          name: "Productive time",
          data: [
            5, 25, 45, 30, 50, 70, 22, 30, 40, 35, 5, 25, 45, 30, 50, 70, 22, 30,
            40, 35, 5, 25, 45, 30,
          ],
        },
        {
          color: "#24629E",
          name: "Idle time",
          data: [
            40, 35, 5, 25, 45, 30, 70, 22, 30, 40, 35, 5, 25, 45, 30, 50, 5, 25, 45,
            30, 50, 70, 22, 30,
          ],
        },
      ],
    });


    machines.forEach(function(machine, i) {
      let n = i + 1;
      Highcharts.chart(`ACT-TRG-${n}`, {
        chart: {
          type: "solidgauge",
          height: "24",
          width: "24",
          backgroundColor: "none",
          margin: [0, 0, 0, 0],
        },

        title: {
          text: "",
          style: {
            fontSize: "24px",
          },
        },

        tooltip: {
          borderWidth: 0,
          backgroundColor: "none",
          shadow: false,
          style: {
            fontSize: "12px",
          },
          valueSuffix: "%",
        },

        pane: {
          startAngle: 0,
          endAngle: 360,
          background: [{
            // Track for Conversion
            outerRadius: "112%",
            innerRadius: "88%",
            backgroundColor: "white",
            borderWidth: 0,
          }, ],
        },

        yAxis: {
          min: 0,
          max: `${machine.trg}`,
          lineWidth: 0,
          tickPositions: [],
        },

        plotOptions: {
          solidgauge: {
            dataLabels: {
              enabled: true,
              borderWidth: 0,
            },
            linecap: "round",
            stickyTracking: true,
            rounded: true,
          },
        },

        series: [{
          name: "Conversion",
          data: [{
            color: "#24629E",
            radius: "112%",
            innerRadius: "88%",
            y: machine.act,
          }, ],
          dataLabels: {
            enabled: false,
          },
        }, ],
      });
      //OEE
      Highcharts.chart(`OEE-${n}`, {
        chart: {
          type: "column",
          width: "26",
          height: "24",
          margin: [0, 0, 0, 0],
          backgroundColor: "none",
        },
        title: {
          text: "",
        },
        credits: false,
        legend: false,
        tooltip: false,
        plotOptions: {
          bar: {
            /* stacking: 'normal', */
            borderWidth: 0,
            borderRadius: 2,
          },
        },
        xAxis: {
          visible: false,
        },
        yAxis: {
          visible: false,
        },
        legend: {
          enabled: false,
        },
        plotOptions: {
          column: {
            borderWidth: 0,
          },
          series: {
            groupPadding: 0.1,
          },
        },
        series: [{
            color: "#24629E",
            name: "o",
            data: [80],
          },
          {
            color: "#DABB2F",
            name: "E",
            data: [70],
          },
          {
            color: "#548646",
            name: "E",
            data: [90],
          },
        ],
      });
    });

    //Table
    {
      function sortTable(n) {
        var table,
          rows,
          switching,
          i,
          x,
          y,
          shouldSwitch,
          dir,
          switchcount = 0;
        table = document.getElementById("unpinnedTable");
        switching = true;
        //Set the sorting direction to ascending:
        dir = "asc";
        /*Make a loop that will continue until
      no switching has been done:*/
        while (switching) {
          //start by saying: no switching is done:
          switching = false;
          rows = table.rows;
          /*Loop through all table rows (except the
        first, which contains table headers):*/
          for (i = 0; i < rows.length - 1; i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
          one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[n];
            
            y = rows[i + 1].getElementsByTagName("TD")[n];
            
            /*check if the two rows should switch place,
          based on the direction, asc or desc:*/
          if(n == 1){
            str = x.querySelector("div a span").innerHTML;
            x = parseInt(str);
            str = y.querySelector("div a span").innerHTML;
            y = parseInt(str);
          }
            if (dir == "asc" && n == 1) {
              if (x > y) {
                //if so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
              }
            } else if (dir == "desc" && n == 1) {
              if (x < y) {
                //if so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
              }
            }else if (dir == "asc" && n != 1) {
              if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                //if so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
              }
            } else if (dir == "desc" && n != 1) {
              if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                //if so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
              }
            }
          }
          if (shouldSwitch) {
            /*If a switch has been marked, make the switch
          and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            //Each time a switch is done, increase this count by 1:
            switchcount++;
          } else {
            /*If no switching has been done AND the direction is "asc",
          set the direction to "desc" and run the while loop again.*/
            if (switchcount == 0 && dir == "asc") {
              dir = "desc";
              switching = true;
            }
          }
        }
        var pinnedTableBody = document.querySelector('#pinnedTable');
        var unpinnedTableBody = document.querySelector('#unpinnedTable');

        let odd = (pinnedTableBody.rows.length % 2 == 0) ? false : true;
        for (let i = 0; i < unpinnedTableBody.rows.length; i++) {
          if(i % 2 == 0 && odd == false){
            unpinnedTableBody.rows[i].style.backgroundColor = "#242424";
            }
            else if(i % 2 != 0 && odd == false ){
              unpinnedTableBody.rows[i].style.backgroundColor = "#2c2c2c";
            }else if(i % 2 == 0 && odd  == true){
              unpinnedTableBody.rows[i].style.backgroundColor = "#2c2c2c";
            }
            else if(i % 2 != 0 && odd  == true){
              unpinnedTableBody.rows[i].style.backgroundColor = "#242424";
            }
        }
      }
    }

    //DownTime Analisis
    Highcharts.chart("downTime-analisis", {
      chart: {
        zoomType: "xy",
        height: "180",
        backgroundColor: "none",
      },
      title: {
        text: "",
      },
      xAxis: [{
        visible: false,
        categories: ["Downtime Analysis"],
        crosshair: true,
      }, ],
      yAxis: [{
          // Primary yAxis

          gridLineColor: "#808191",
          gridLineDashStyle: "longdash",
          labels: {
            enabled: true,
          },
          labels: {
            style: {
              color: "white",
            },
          },
          title: {
            enabled: false,
          },
        },
        {
          // Secondary yAxis
          title: {
            enabled: false,
          },
          labels: {
            format: "{value}",
            style: {
              color: "white",
            },
          },
          opposite: true,
        },
      ],
      tooltip: {
        formatter: function() {
          return "<b>" + this.point.name + "</b>: " + this.point.y + " %";
        },
      },
      legend: {
        enabled: true,
        borderWidth: 0,
        align: "right",
        layout: "vertical",
        verticalAlign: "top",
        x: 0,
        y: -15,
        layout: "vertical",
        style: {
          color: "white",
        },

        labelFormatter: function() {
          return (
            '<span style="color: white">' +
            this.name +
            ': </span><b><span style="color: white">' +
            this.options.data +
            "</span><br/>"
          );
        },
      },
      plotOptions: {
        column: {
          pointPadding: 0,
          borderWidth: 0,
        },
      },
      series: [{
          name: "Production",
          type: "column",
          yAxis: 1,
          data: [12.8],
          color: "#24629E",
        },
        {
          name: "Breakdown Mechanical",
          type: "column",
          data: [9.8],
          color: "#B32F3D",
        },
        {
          name: "No Material availablex",
          type: "column",
          data: [5.4],
          color: "#548646",
        },
        {
          name: "Machine Idle",
          type: "column",
          data: [2.5],
          color: "#B6B9BC",
        },
      ],
    });
    // } catch (error) {
    //                 console.error('Error fetching data:', error.message);
    //             }
    //     }
    //  // Fetch data when the page loads
    //     window.onload = function () {
    //       fetchData();
    //     };

    document.querySelector('.document-download .icon').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAHSSURBVHgB3ZVBSxtBFMf/7+2atZIthZwqFPbkqYVCTvZWeurB0kpLe/QDiNBTwW9QqAh+gupJPaio4NGc9OTNk6ec1EtQiBqzycxzdmPWaDZxjIjoDxbmzUze72XyJgs8dejmxMzMZ0/eZN+KokCDcu0f4HLonq479f5PTDRNTB9/f5svdhJwa/B35YuvBv1RpTifljxCoH1UBnzS/MqEgWjZnF7+FVgJuDaQF5CPu9FVwq3VOyRD6I2OkkQgoRfAEteTrLA+tpEkAhfIwBJWNOg4YcEMi7dJGD0g5igr8tKJOsgEsyIoXMqiB0rp97gqvDc8CUf6IFvj3xfGuu1L7sG/xR95Zs6nbiIMm7r3RKjUvspVhjrRQNicOef+rcnRuXiv1REJMGbadzh9VXuNO0Ovm0+mXgkSPR6YxxVEZ08kuU6xDV27SENG4sSIfgf5YEY5Av034TYs4e6LPNUcU+PPb830vHXya4J6S5s1EZFSJDHVRy23Zr7GOuySJrmSI8q6L/ZDXUWaxFQ/iTtQy1SLLbIG4/HFkAPcExLa+/N1tdwmiM3ueYEgZfRI/LbrO9u5PpfC1NLPIdTkHTlk1ZKipCTkFjOHR7sTExtVPCsuAEFPspbGi68KAAAAAElFTkSuQmCC";
    document.querySelector('.header-logo img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG4AAAAgCAYAAADzCU3nAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAapSURBVHgB7VpRUttIEO0xyW629sd7A3GCmBuIv11IVaQTYE4A/gLyY/mLJD/gEyBOYLkqkP1DnCDOCVBOEKdqk00g8Ww/qQcJIRkbbNhU6VXJGmlGrdb09JvuGSuaELs9x1Lq4oCLkdaPOztuEFGFB4O6qYHXc+pPaucbpNUmX9bldqT0qLvlvt2nCg+CsYbb7f1pK7UAL7OINHtabZ1oxJ5HbX6U78Xet1x53/2j0HDwsl/pYp8NtIZrrVXnG/2z77nhENdsUItowTP1LGR/pB93KwPeH64Z7nX/2cZIa48SWgy1/rG+4/4dFT0MAypVOzHex4b0tp4fHVKFuePScEKLTIFk8zFUmjpb7tFEc9hu7xl7n97gYl2RDkb6l1blffOFAi3+pi7amgjBB7G3dc/pi2docVLk6ZOU9rafH3eowlygXgarZ3y22HADjhzZU96EZY13eyvNWk3pcXSINiZ4YXcebDlHS1Rh5oDhNIy2c0MHJwZRiDARrCyPM3CcQqjzd2DibedokW4BrTVomgMkFeXu23xqx8ortVzwzFM+fK475OuTG15zyu28nGxQfkNuRUZW7j2Y//dyssKCdhKRx9/Rz+hXhiG3czPvgC5ORpfAvOMRftgzJqDFGj+opfw9GtfSc4Phy2CFbotMBwKbuWqLknm4CE+l7lSuy9oZRJl3opObRe/SSbC2nBlE9YK2TW7HwZrKTg+2yDjM6XejPgwMukbmGmWH37GIwfaIJgQ8bDdYHSitP5RFmTNEW85r6DRWdKr5NgM/U8bIRYcP5ABC/PA74D1NuRfx0adkMD+V5yw+TrjdUk4XlF2ph+E9bnOYZ4kM+pQapyFHZPQQedDHodRovuiLezYfmNqo1HBY4qqp8z2taGCCjNgzlfpo2rzsr7R5XuQR+Xh9VlGkKG3LJToaHufRLcAduJ6Ra4ncfo4eLUq9Gp3ezOmDOhjWKtAF1BZKO0OxNl0dMFl99jNyPWkfZfUUpCtUaV0XTGTeV6NSfPM0KYcN473urTTytUgfUJcoet6k2SE/d2wI388LxmhR3miAdLahuo18PQwv1J6dF++KSM6QfSDyyRgNKDVcjRYCLHNxcThS6uRV76/4AzlPq8PTOOcDBw+TY+TTDMAKNikZ2YAr8o3XzQsmWHg/pk0g53puEFmUUFc2CBrQHSEGMoOlSQlNnwkbxbhmOI4yT5AiKBpF287xIkeFf3AU2dWqBk+wxQs3OUFvoS6uJ9XgZz6+Clb36G4wcxsiNHRWV67n7XXAxzF108yxbZoBxPtBk2YgWHz0hLqvGi5JopNISKtUAQ5MeNL9gbA+QurwVX9eNKsqCP152Qtt6/oOnpHzNhOZ+XKep9cZT3PGDI6mnKN8oKQEXGzJrc1ZDTIWi1QEaVrc93I7tssVw8XRInsXYY2SORuehyDlsg6Ks+Eyi832E3XxDvQJg8IL6fbIelsoikeUUsaanC+pyHC/lC1KDX9Gk+OSBqnAW4SezLvDMiHZwIPS4OLWyNKi9IPpW9C1dS2q3HbfxCM7XcK6OGMK3P9Xf77MT5Jlsu8HmrRz2yWynJJNuu5tBj4lHYeJek0S65ASZjiQpPYTJYEDZECPU5oQGCQsoyvPb0qHGSPYdDUB7hTobkvxuWk3Jh2YCCan5HMkunygNDCK5ZemA+JhTV4xCTkiaT9Rv5sPsNjLzthoQ96fW37hHoV0d1zzNgPpWNyzKaEseCC4HwGBRWknG7Sm7ThuD4OhuFEiM6KrCXgW+dWZu7COwSc5WwW6xPO+ocqGocQ8dtxjX+vRMn/XqQiyEKx81V+Wypa94lSB1ER0ISM2ooSGyj66Q2mybEkHYrkLRhzI8z4fSJD9EhnvRUZUVAnjUTIg0Eai5bjcEbn558Lc4VNi3CDTZjDmnZHUvS/RBd8XUBoYoX3LULKSLRkz4v1x/ydBxAkBHEmuF9Vn/pdi05RbQxWmQy2NGGM+5YXki5NX/dW1aQVhA5af5YXl2GjYgF2qjDY/xHOczGeLZkOUadFn77In+TdXTItqYY+DlAaMz+f1F+7bgCrMFbl0IPa+JTYc5g543zuzYpIHIksk3FhB4RShgegS815ltPuBKqvIbohyRNDfdo8dM8dxgHDKkyRWSRCAhPgz0bj9uQqzx5h04NhnGuREvLa5QFciKkeWYxB8tKp57GGgpmksHmcrUsFIf2/dw75chRJMvJF6CaZN9jKXKjwoajQtMhupFR4O0xuuwv8CleF+UkxlON4w5YVl/YEqPDj+AxPhSeKFIDliAAAAAElFTkSuQmCC";
    document.querySelector('.header-company img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAcCAYAAACdz7SqAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAGVSURBVHgB7VXRUYRADA03/nsduHZACViBdoAdQAdHB2oFchVgB5wVYAfHVYBWEJPZIIHhZBe580PfzJsNIWz2bbILwF9BADOBiOZrkiCo4RSgJGvihljiONgfcxwsAZooITbohv1U4ouJZPxxQYzE9UasxX4nHogck4hvJ++NxPpBtnM/oqRR/krs9vlRxTyg71bTBwV+j5eRuBL7Na+cE6Ot4RRYTYb93agkqa5/Npw/GEloaCjB1sUFrPggNquKwdZ1KzYfqWv9wWpkkkQScqNMNQO/fyVeynNNfBJ/LAuaBnaNkaEbGuy2sySGaq6UeDuVMJKE7eiDZ5gDtA1kiDn6w8BcSNJ223zgfCbHGikD2xDHmqg+4g9hLrCrqUap7OGlwfH3EuOkdjVIyKvl82WUe6vsmvgx8Gd0DnPiDdhz6q0ylDFXKoxSFWN394Y8wgz0/jK02raOV2AvB179nfhqsBcBq0nl/Q6WgqhJxS6UynWrTuwIloLeNmkQ/ZzBKcCqBkljOCco4QbODfzJ9faP38An3n8GNvpm/DgAAAAASUVORK5CYII=";
    document.querySelector('.custom button img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAGkSURBVHgB3ZQ7SwNBFIXvnTzwQTCQzsLfIFhZ2vuIigo26h9Qg9jYaCVYaLS000oRJCkCadOlEgutUlmJjRhYVEyYOc4mWZOJ2Tx2FcEPlrt3zuw9M3eGJfpLjlJLG4fXi5tedRvRToSkOBHPeNVtuNVgMhWPUrkvjqDaIHAUCuctPxa8UolAggKlXGI2XexoYG+bFO3p1yj1xgMIJ1vzV8euBsnU8iiUvCUfSMUT2wuXOSc3zkBJmSSfCIFdI29MmGmUfKJbYtQINiYKWGMleu29AYQquhqEH63s+2AoTD7ofy2XXA3kcGQuRBwhH8ihQUuHCyc3zgA+i1drKKOGoF+mrYG+VeP6WpxWogaMfftppXky+AmC7USA8jrkUcsZvOOmudGhRYjpNkxWIlXb4rSkWfO2A2K72BSqv6yMXu1qTco3a10ZcAAWZP2qMrEuhIZIZ1Xj71p9UXh2NZBlFITgsa/JqEzOOAVqfW+pOQgIw8A4gxGiOyZY5BEmYZVCbzfmWBMH6emIKA+MCaViHOBYd4VhSRUshJ9e7tfXsx/0r/gEFHmrDzEJ2WsAAAAASUVORK5CYII=";
    document.querySelector('.maximize img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAFVSURBVHgB7VXRTcMwEH1nGKAjpBOUEVgBKkL/kg2Q0gGSDtAIJiD5K0WFFToCTNCMwALkOCfEGCc/JEGiEu/HsnO+ez477wHHDnIX1rtFCHAgH86t5SKaP0ztuHR3fZDBa+YM7Ik4iy63uR13+n2TH0towoy9TO+Y+a1icYKDS4RBK5SlZ5gqCsCUpc++F11sV60TaOYEvtcbl/NNgh5YPy4SUhyDOGxOokwlLm9kKPom11hebarTM1PYrJkCIDrjknMMhLTk1b4/hZHR3NuvFXBhFyi6XsvPM6pC58I//h60tqRPfoCRYb8ij98xxUBoyfkUwlaBcVALoNdZQOR2gpFh5LqSaFIzDEQt23hp5rbY5VqkRM9j9ETtJ9IexbcmrR1g9Fx+dVtZtYS4TlXFa/eTnjPRhAgzTdD1k07LrLxB5NtabllmzfjLNnWLhVzWReS48QFOnofUyRtaUwAAAABJRU5ErkJggg==";
    document.querySelector('.settings img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAIbSURBVHgB3ZXPaxNBFMffe+yPBI2XBSFFMafcPPXUW08FD4JUjIriXyAF60HwDxA8RKF/gihqij0VBE+empMnb16MtDSnILhqsrvJPGeyXbPJvLTZY/uFkOTNvO/n7czOG4DTLjxucGvrmp9cunBlGOHvJ3ffHebHnr+9s3TO5/MXh6rTaGzHhQHNndt1VLQCoHzzP0lwN4OkY7CaGlDokvrycP39t4UBeYNMjBj7yHuR/k2KlxmwMpXkqPbmje2vs16OBMAR1GbRyOzFDKsmzEJdaohL+ssCkASIWXWhoEqE36W4CEAv6kBB/fnZ7y4McGO/JsX1RlzWq1MXcwI55/9iPtt5EJQwWkbggEfTG4jIgQJ4jIDBmAPc05U1mbE3XS339Lx4QKW9p+uvemnsSGU1WAO9ubPmRnnztCocA+15Zg5WyypZm0Czpwfb+MitnjfPQ8gsmSAGVbEA88X9eSOKuAwniCYVcSj7475ZcytsYgxzTi+FFiB2Brs6rcsj24yAmnlIusnUtI11kQydPrmfJjFBL1r3rgINV4TSzH78ZeYDa0w57c3Gm8VOcuKFHSlulkQ0NzmhnCMCHFWuQkG5Ffmgya3CNLuCUjRudosBgB2hr1DEBJ/NR3rjyOFDyerYCyff9z1tnF0qLz/crzIn1zOwR6pd6MLJ1Grd8vYdqhG74aObr6eeykAUJhX34NePjY2PEZxZ/QPMFtuNUsBZNAAAAABJRU5ErkJggg==";
    document.querySelector('.production-count img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAXCAYAAADgKtSgAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAHySURBVHgB3ZVNSxthFIXPfWeMNG0gMIGSrEorEVoKRboodlFpU6jQpimlpcv+gW4LTSmkm0JW/Q0u/VhkKWogK7MSIoKCoGTlThACSj7mvd4kjo6Z6GSiC/FAyMydOc+5dz7eAW6Cpn+Mjabz6UgQD/md8C3/NsE2TWhComvgGoO2538trQ0NT/98GbkTvfvKgXqNXCPWa7PZ4vbA8Pb49+JjEyD9FAOoHdKoN8uFXKnqPebSl/y7cbL1CyaMIqhYbcxnF8vukuFsZP5PRc2GSg0F7rTJ9x+nHlqbxd0dp6ROU45CH23m5wxYCChmHWbGVzDNZnJTUadungVzTJ6CEGs8AHSECHsg1fAH44O09lq2wu39kfCI+FE5Bz9ngLLEZMkN2SPo/X4hDE5Kp9/RmZTP6nX2dn5BVwmZJtaeQoL2u1AJZQiUkvCRCV91L5V0miCl4pL4mTGYFAYWhQQ8iQAKAA+uU7iMfYhrUMs2qx64DSrj6qoUcotVD7zwe7kii8EqhhYftBrqk7tiuHe2ijvrT1KP5BLpGIHCvXZ5JMflz7umM5daTWPa3XXnfPRR5t8by4DxXlIme+DyNiLuhkLz34U/K6V+nEs/Fr0hDlxMVfnl5rJLM5f5fb9EJyFJE0ZSA88M4lqz3pyR9fsAt1bHNe+sHvTuTDIAAAAASUVORK5CYII=";
    document.querySelector('.productive-time img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAIwSURBVHgBxVQ9bNNQEL57MQQJUCPBAJul0r1rN7KDMAys2IlAYipdWYCFtTAxNQ4TAwNIRGJsmDo2UiUGFCFPjZBaySJSpIT4fdxz48pO4v5ZVT/JufeX77t7d/eILhJLdX/1Tm3jxVn3DdRRm9BwCPzgrPsGPG/Rdv1KibTDzKsgqjDj41wB4ifyIwNei4jaQdMLjxUwYQN4LcMKnQKiEzDhfbdRf5crsOT6y1DYpiLQUbXbfNpOppkcgLFORcGlV+lpNslMy1RYIMthpSfQ2pMTp7r7WSDMFeA/g+/h7auXqQAqvcEoV0DfuvZoAbhOBSAcfTGfknkmB1JmhcjncSg6Z1hH7jKtMLELQlM6aUvGb03nSCO+TB+Tjt5lHfXiMdA/uUAcICZWz+wa4r+MH3uNWj/fxxQWvY1ndEIY8t9+rSXvlm0x/En9S4nj61iP1oLm8yBxkfLV+YZ894yd3jOex+TmaWG6S4dvFzuWKm/b7gf7WAHBivz5fmxT0EBvr1HvW0qvU+pRBCiQZn1oxhaX/RkBCXv6Lrdk8VtsU1DAbuLtISmoHWFYjajUlqyFyZOR7QMd/coKYl++lrGZc1BJt4fMVInoX6fr16pEV6gkVyaJtZPIMgLlwcLOnChmECncnHjQMUQlVd5cdH1HyDcn5BSX9rTAz8+PR5cYLfFqB5r2cxUUD40Zg704CiFlhS8JubCHkR69iaOlgjioJJNsdgyxLHXGGHpJmZ47/gPAZejSMCRTRAAAAABJRU5ErkJggg==";
    document.querySelector('.machine-online img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAVCAYAAABG1c6oAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAJSSURBVHgB1ZJNaxNRFIbPuTNOKkkkpRubhZSiyaaFYlbSTaRNrKASSuPHqvkD2p3YqjihNhr/hHElGpXxA7TaQjbSVUGwFBpFWihmJcamoJnMvce50amZJkbblb6L4X7Nc9/zngvwrwvbbSb0aEDVtAkgSABQGRkzalV8bOgvVncETM7EBgRAChHG7SOB7fuElONVJd0K7AKOTY9EgfFrgBiFv5OBROn85VdvmoCjM7EUQ7wNuxEXRx9cnSvIIXPWGGAP7FaMRX9xfspiLGfbXYWdiqhg1VjOmTY1ZWx6OAoKm7CHiT+BQFDaKdUFTOpJDbUv58hcv5vXl025lrgx0qOQ0O0D4w2UMhHcscvKOY04dXHQ39Hpi9+fnH0o56r85PW8eTobB9AORACWF+SaMVl/EikbrCtWbYAYC4hazTD0QrnRkafTG7J9aS6HdUeZoS4F98QV4IAkFr+Wv5We3HpdaVXt8fMHPb5gbx8S9Mm5CfyZMTX/yQWs288O+hnv6LbBEQkWllIyK5VFB7wdRAhLm94PS88vvK82OTyTiZ8Q5vrLrQwzQyEJZiD8ZLGiogifQAjaGX60Lyvem5ovbuXv2TjpyrB+mwJdrgx//FAcvX6sm6lWmIO6KTg9fXRltuSqv+NzGEhtzjB5c7gfiB1BDiVkfMVx8DudzcaCxPGwdA0oFvKX5t62zFATeyMILIRAFdmcRrDMcN/+3kMWo36FUOPMescFrjgNaQI2gj3CGxI22GkOqqLSrhltgY2qN0coYWQyZl7c8K2ttQL9P/oO9GAGuP8S2b8AAAAASUVORK5CYII=";
    document.querySelector('.down-time img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAVCAYAAABG1c6oAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAFTSURBVHgB5ZMxS8NQEMfvLm2kQrtkk+pYodBWzCSt4CiI4OTqVyi6uvo9nJ2KxtFJxcmhCIX2A+jUqYIkJu+8PDC+kGJDddH+l5cc//vxf487gH8hr9nuXja3u3m8hTymCGjDQuA8XoJf1gIC0fy5bnVqctTk9VfMOgMeiROR+dysK1JjS9HTXv9ulEnobXZcAe0kMOZlZnYyEaSeNCty4p5eY6uRAl7U67bMhpvOjlVAOpWGVaNYZaQzYGqZVrLQ1YxPYKXkZJMAjGT0bgRyLN9rcu84zYmuoeqngEx20a7om+nBjkIuw3R58TzLG+7rfMBXcW2a0eaCnQBnyDOWxJtlzrV6eUABhkF86jf0/WAMPxS+WV/AkF4nMg4+zCuGycHw/jkBHg4GASI+wJx6pzDpTW1Kb71dtkroslIOWWR/B1GRClDRuLiEw93H2xf4M/oA4oFpBmzWxvcAAAAASUVORK5CYII=";
    document.querySelector('.idle-time img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAVCAYAAABG1c6oAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAHgSURBVHgB1VQ9SxxRFL3nzuxuZmGb2AWLBMzEDQluq6SKH8WyafMfbNLYprQ1vyG1VUCwkG2DIWCIzEJcSAhiwGqrJWscM+/kjlGc1R2d7fTADO+9+9655953eCK3HbguuLk5VXnkBc88+GECiUt+0v1bOt6fftHtFyZMScKg+tgleEjKg1GHnOOheOj+jH/vN5vfj3MJf7WnJ/7IvVZCVqQAoNpPWNqqL3zuna9pdsMAwVJRshR0rubhZCm75l/eMDQXViGYt9GsEEcADwDZcEQv74xKbj2cgOhbY20o0FaVjxQJSV1Ri+Ud0/wAWhBnwrhm/11HSyG6SvLIFM6PTZgqE2DDSh7YF6alQziAStvGjfEJz2AkDZP22jJ8/Z/IeikMxiZ05iLz4RwhB/atW//WTxOAM3ZZu2MTWsfeW3DS1L0ys/ZOlYosp61Ib7oQoQdeuN6s4cBVk3TflK5YhmWrPzCytaxtQMRZjiEfnpBf7HZns6RW6ruLuUgilxT5SZSrsL4YRU7cthRAWo0Tf3vqZbSTXR/52ux9eFLTaqVFoDYqDrJf1XhrcmGvdyUmOeh0npYrh+U5M3WYXS972v3W6X9qvhl+ZW4kPMeP9kwYk889euUE2qkv7kRyp/EPAn/CMoktx+YAAAAASUVORK5CYII=";
    document.querySelector('.mhr-time img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAVCAYAAABG1c6oAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAFTSURBVHgB5ZMxS8NQEMfvLm2kQrtkk+pYodBWzCSt4CiI4OTqVyi6uvo9nJ2KxtFJxcmhCIX2A+jUqYIkJu+8PDC+kGJDddH+l5cc//vxf487gH8hr9nuXja3u3m8hTymCGjDQuA8XoJf1gIC0fy5bnVqctTk9VfMOgMeiROR+dysK1JjS9HTXv9ulEnobXZcAe0kMOZlZnYyEaSeNCty4p5eY6uRAl7U67bMhpvOjlVAOpWGVaNYZaQzYGqZVrLQ1YxPYKXkZJMAjGT0bgRyLN9rcu84zYmuoeqngEx20a7om+nBjkIuw3R58TzLG+7rfMBXcW2a0eaCnQBnyDOWxJtlzrV6eUABhkF86jf0/WAMPxS+WV/AkF4nMg4+zCuGycHw/jkBHg4GASI+wJx6pzDpTW1Kb71dtkroslIOWWR/B1GRClDRuLiEw93H2xf4M/oA4oFpBmzWxvcAAAAASUVORK5CYII=";
    document.querySelector('.time-analysis img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAI/SURBVHgB1VQ9aFNRFP7OzUuUSHBwSkVwyiYKnbIVXFXU0Fed1F1sNYKLg0WXDknj5OqgVtTaIgVH3ZyeBnQxXRSEOFgsBFuSJvd47nt9YvJ+8tp06Ude3r3n3nu+7/y8C+x30KBhbuHSWMZCAZbOc49ySACCaoG52UlvOHfOv2khiuDh0uR4r6fGMQJSKe1MX3jp+HPlD+aWz+VGdW5gfJgsBAjUVnZk5z7cFA8SWKTz2CsQ5QMEjGQF7QPTsVAzdC5AsHPfOAXFd6VNTsbtszAE0man5VWQQZ0ZH1wb8REGbHnWpG9WCTryfGwERCiKUlsTjMOrME6Js+Ku7B1WFWK9gd1EYFRiWyUxPZAIzshzVhQVpV5ZsT9i5jUMQSiBUcmeyk2jkqVs8lsRggZDFQnckHnd263azJ1VjXTGdOJgs4RHwFQgLy39KhkN+ROS7SjliuhYf1b868F8rOnuoYuAPuAfCa2BFLZOoPv0T2UYVDuttHv3VBan3lVf2Z9cIq0+mrX4CIxY5h9Ra1qnnMzP31+u33jbri5NTUsTTZiOqCzaM7dKT2uy5XNsBHHQ4O+37WfO1tHDl41yMdT8NYl63o3mtX0lQEApbiVj4F+eM56XVE4MLrs2pscBgm5XN5EASpF7U0pnzUbtkU6aDRBY6uBXJALlq8uTJ8qlFzUpVD3oHO/Lpef3AgQ3S0+apnhIAN2lsUh64Pj/874iu8WzNhd6TA3J8dCaSCrW5bUuXmZkfE3G3zi2tfcj/gK/Ld+79nO8+wAAAABJRU5ErkJggg==";
    document.querySelector('.production-count-2 img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAXCAYAAADgKtSgAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAHySURBVHgB3ZVNSxthFIXPfWeMNG0gMIGSrEorEVoKRboodlFpU6jQpimlpcv+gW4LTSmkm0JW/Q0u/VhkKWogK7MSIoKCoGTlThACSj7mvd4kjo6Z6GSiC/FAyMydOc+5dz7eAW6Cpn+Mjabz6UgQD/md8C3/NsE2TWhComvgGoO2538trQ0NT/98GbkTvfvKgXqNXCPWa7PZ4vbA8Pb49+JjEyD9FAOoHdKoN8uFXKnqPebSl/y7cbL1CyaMIqhYbcxnF8vukuFsZP5PRc2GSg0F7rTJ9x+nHlqbxd0dp6ROU45CH23m5wxYCChmHWbGVzDNZnJTUadungVzTJ6CEGs8AHSECHsg1fAH44O09lq2wu39kfCI+FE5Bz9ngLLEZMkN2SPo/X4hDE5Kp9/RmZTP6nX2dn5BVwmZJtaeQoL2u1AJZQiUkvCRCV91L5V0miCl4pL4mTGYFAYWhQQ8iQAKAA+uU7iMfYhrUMs2qx64DSrj6qoUcotVD7zwe7kii8EqhhYftBrqk7tiuHe2ijvrT1KP5BLpGIHCvXZ5JMflz7umM5daTWPa3XXnfPRR5t8by4DxXlIme+DyNiLuhkLz34U/K6V+nEs/Fr0hDlxMVfnl5rJLM5f5fb9EJyFJE0ZSA88M4lqz3pyR9fsAt1bHNe+sHvTuTDIAAAAASUVORK5CYII=";
    document.querySelector('.machine-name div img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAVCAYAAABG1c6oAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAJSSURBVHgB1ZJNaxNRFIbPuTNOKkkkpRubhZSiyaaFYlbSTaRNrKASSuPHqvkD2p3YqjihNhr/hHElGpXxA7TaQjbSVUGwFBpFWihmJcamoJnMvce50amZJkbblb6L4X7Nc9/zngvwrwvbbSb0aEDVtAkgSABQGRkzalV8bOgvVncETM7EBgRAChHG7SOB7fuElONVJd0K7AKOTY9EgfFrgBiFv5OBROn85VdvmoCjM7EUQ7wNuxEXRx9cnSvIIXPWGGAP7FaMRX9xfspiLGfbXYWdiqhg1VjOmTY1ZWx6OAoKm7CHiT+BQFDaKdUFTOpJDbUv58hcv5vXl025lrgx0qOQ0O0D4w2UMhHcscvKOY04dXHQ39Hpi9+fnH0o56r85PW8eTobB9AORACWF+SaMVl/EikbrCtWbYAYC4hazTD0QrnRkafTG7J9aS6HdUeZoS4F98QV4IAkFr+Wv5We3HpdaVXt8fMHPb5gbx8S9Mm5CfyZMTX/yQWs288O+hnv6LbBEQkWllIyK5VFB7wdRAhLm94PS88vvK82OTyTiZ8Q5vrLrQwzQyEJZiD8ZLGiogifQAjaGX60Lyvem5ovbuXv2TjpyrB+mwJdrgx//FAcvX6sm6lWmIO6KTg9fXRltuSqv+NzGEhtzjB5c7gfiB1BDiVkfMVx8DudzcaCxPGwdA0oFvKX5t62zFATeyMILIRAFdmcRrDMcN/+3kMWo36FUOPMescFrjgNaQI2gj3CGxI22GkOqqLSrhltgY2qN0coYWQyZl7c8K2ttQL9P/oO9GAGuP8S2b8AAAAASUVORK5CYII=";
    document.querySelector('.machine-status div img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAGdSURBVHgB5ZXNTsJAEMdnto1EUxLjR6InoyF4Eb96EeTmlQMnn8an8A18Aw7wBmq9YFAICSdvxgMaI0YCdndckK92t6EgMTH+kiZtd/4zs9OdKcBfB8Ma5naPt00GR4DYdl1eyZadchhdqAD5w7QNHGzPSwHFTPmyOE6rBDiPxSJb1qpNwljuGhhgSWdRrZpBgzi89++b7Zfr02q1PWpi+jWb1loSCOKIg0yDkYGl3XdwgvVoZLEl7xxvDhoRTInL2Yr/HYMJEMhOgGh/Eo1Sol7NtaB0Togkq1fSrne0Pjw76J6WH5Soo80n0rY2QC6RTChHcRoY2IW9dHz42MM0jR2YERxFSgkAnOZgRjDBWkoARHTGCRGoJD/w3Ti7T+Y6Q80I2pEwCtFCL5uPQBvfCPGcosytXJAtH6hleEbygsAEoOGfT0qjDWaLBkZYk07qwf5VrQkTQReh53s/KdWHqMO0aMqr7KDJX4vzxlKk39EyYwtQ392CxDMC645nNOHx4e2p4rf5/R9OEIWD1AZxJjuU5lyAm+z9VQ3+BV8+p4qLWxhTBgAAAABJRU5ErkJggg==";
    document.querySelector('.machine-mode div img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAGhSURBVHgB3VWvT8NAFL52NTSZIhVbmK2FpAqNnoT/AY0j0zg0GkWCxlajmswy12zJRFO1ZDP9wffItdyu79Z2KPaS5u7eu/u+9+N6T4j/LpauSJJknGWZ7zjOCMuh6CYbnFnjTOR53sZIAPAAQyD+JkQSVQtbAR9y4GVZTkxIBlsQx/G4QYAQOc+v8M0AdK4bLMsi3QzjpW5zXddvEMic6yBnokWKonAZ9ahBILoX9EcQ1bV0YoK5TjLkCPrIHUCnkugGw71p40ECHE7ldKfoXAmqim+6DG0RLPA9Ig3b+oBts3Ux1csRBqGbQ55SjrFcYv5BRDIqIvaVvSlsi74ED/Iq0pzAiOhZml/w3ZIdt2g1GAxC7BF9CPwKXNVRVNCnMmWvtEbKUhM4iW3wfmfYv1dsDE9CSVVnAni4xLDSdCFXbO4vbyWQQvmeV+AY3sURohLsP7PwFl7OpZeh6CEofMoRrPWNB2rx66FtbxldTVD3AzzX9EBNGYwLodWjg+2tajx1BFBQBBGz2QTO2vI8/1S7Gtcyq8ZzTMv8ko6ekHwDGHe3mODvuecAAAAASUVORK5CYII=";
    document.querySelector('.program div img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAG+SURBVHgBrVTNMgNBEO6emQTFIRzExkqFKiLKIS+glAs3hYubcvAAHkHxADwCHkM5uDtwULZcbMRWIieXKCrJbpvZZMrmj2zkq9rtnune3q97ehpgwECt2PZdLMKHt/SaR+DGMDI5CAk/YNG2UiDgTi5jzWZaM2YyN60fKX+Pwb5eM85sw0xfKF2ol8cpy4DF2n5HuOs4T+9tLDw6ZgibP34ETt5CM5k59wOyNmY6HowhsjgRRRnQBCF7q++7UegC0Ug818mIiNeJ6fkr236YGopE1qelrvYLzuMSEJuS9dJEcqrm9VANFPPWPjBcDaTxIut3pFQdMGGmL+EPCK0YMn8pzuGfENAHVIsJGM4G92Zm690QKqBKXckRwU8I8CBoK75afouFCqjqqKTskrR8N9lcglRohvpQ5ClPILGVoC3K+L2SDPpAwlw6A0Gz+vmqfY5Pmgv3oRkG0e2e98XwN/TEcPQDyjV5J0ql53g8PleSNTyUNTwNuOQ4sm2Vdk8MJ5eXyzWPLLda3Sg4T3tIfKfFJVXx3GzPDBWSycVbyTCvdLdS2cAWu+DoTyWEPuDPQ3/kNaaUHC6d5uZA8A0tApLM2eEv9wAAAABJRU5ErkJggg==";
    document.querySelector('.operator div img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAGSSURBVHgBxZPNSsNAEMdnkmBbS7yUHCwR9FKrVBB7FXrw6juIz+BN9ObbiF4snvUBChWLxhb8wFIPIYcSrbUmGWfBlGBiktJD/7AsszP725nZXYBZ6MUwimKkicUkkKJSDT1QhU0S2ETQ0PW19kRA0zRU54tqvDkyKxecdjbbb2jath0L7HQuMwu5lS2XYANSyEO4zWSsVhCMwaxGI9r1y0sr0YZi0T1DrIyELfmO4ZCWo2Aooc7TARAcA9FSyM97uo/yqm+PgeBAITIDF/YRsISIOsP3omJAcgph4D+SJMjCBEoEkuedEpDF3R54HtWT4pVEIGKTpybxG+Kyk8LjgZzRPEN2GLMpMuQli8fF7zw5UJbwiO/x72WVuAWHyUAl8tQBV0nBBS79MxTlKVYIqOvuQ+9VrjBg/BaZdCJ6Fyd22x/f1rNvj29ZvHR7gHUXoA0pJfPXu2k9nZfLEV8vKMMw1HyeqjL3K8rPVfT679Z1EBQL9NXt3pf4a1X9Nojy5nJ0pWnrbzCNTPNuUQyYhX4ArO+ctBo0oS8AAAAASUVORK5CYII=";
    document.querySelector('.downtime div img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAVCAYAAABG1c6oAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAFTSURBVHgB5ZMxS8NQEMfvLm2kQrtkk+pYodBWzCSt4CiI4OTqVyi6uvo9nJ2KxtFJxcmhCIX2A+jUqYIkJu+8PDC+kGJDddH+l5cc//vxf487gH8hr9nuXja3u3m8hTymCGjDQuA8XoJf1gIC0fy5bnVqctTk9VfMOgMeiROR+dysK1JjS9HTXv9ulEnobXZcAe0kMOZlZnYyEaSeNCty4p5eY6uRAl7U67bMhpvOjlVAOpWGVaNYZaQzYGqZVrLQ1YxPYKXkZJMAjGT0bgRyLN9rcu84zYmuoeqngEx20a7om+nBjkIuw3R58TzLG+7rfMBXcW2a0eaCnQBnyDOWxJtlzrV6eUABhkF86jf0/WAMPxS+WV/AkF4nMg4+zCuGycHw/jkBHg4GASI+wJx6pzDpTW1Kb71dtkroslIOWWR/B1GRClDRuLiEw93H2xf4M/oA4oFpBmzWxvcAAAAASUVORK5CYII=";
    document.querySelector('.cycletime div img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAIaSURBVHgBvZRNbxJRFIbPuXeGUSJKghsw0GlidGVS0xWrdqmL1sEG1J0rl9p/0P4G/4G7msYOH01MXOHGrrowcUWMAVHcFNNIU2Q+zvEOhAmF1CCSPos7mTs3z7z36wDMGQyamy/vG5+fv+0NO4N3L30t6QGYKCmJDJH+YIQT36c2olZrWjutc4Xp8sYT8vx33x8W26Ztmb6UWWQZIaIG6vJIQ7dDEhzy9ASQn0JGk5kaXXA/HFnlzoQwU8o/Y+7tC2ksMMEdkHAY+XLyaTT1KNeL67Eo66ss5RXynH6QCSEitQlUqrEBf2Ohks8Sw+0u9d4Mk4rhR2aRQJ9rYBgJ035sTiNsrO0eoFrTIO2wry9EojYjdVjSLeF7yyzdZZiSU3SrIEQyXVxPwbzI2BtrwfTDhP8LoaiT2vlQmKk8ehG3rTjMiK7rv9QuGKEQiLdjwliBOTAQMhwj8SLMiOu6VwVg/9hoAyGXVGvCjAi1fsGVDBN+ze1uCnC3YQaCW6OMSUSnFgoDfNCtdLlgwz8SFZdXGLnetMqtM0IJsqqqymqmUtiaVhacPQaOdck5GPbh6ADTzi+RQJsBN5sPXpfOE/WLg0qm1j7h+87+RHE4K7Xi9VzxOFMqbDHCkipVVSL8+C23U72xZyWkZmSBKQXIrVNy3o+XL21cGMiCpyDxioAW1UV/Kga/vatFRQR+ez/hEh827u39gIvgD+TS9FrN76hdAAAAAElFTkSuQmCC";
    document.querySelector('.tool div img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAEsSURBVHgBrVLRcYMwDJV0DJAR/MEAGaGbFOgCyQRNR+gAgWSDdoNukAzQXOgE0P8G9QlwoE7So3d9h0+yufekZ4sogEuKWR8dTYCEB5E0RZzlVSR69GJj8fBMrqmqUm2x3KS1P4uz4hGiJlxZflNAlT+J9Y2U9r5aR9AVhJ+xtpb7f9HYt+FQpMlY0JMtZ+aKqFFkPy3AdzK0t15ckvWpX8h5Ybm311loZI/WSkiXsHAMye/5A6Job7Lfd2gtKDWOiUsk9s1C8qXYAKYAv5GtQ1a+b4sKbQ7rbCtTyW1l1hoiTpkcn04fwx1MIVu7yo6NDBESmZ8t/MXz+MnPg4bR3cVp/uLFsFc854omIOqClpiSO4jskM+vVb6F9g6+GlliDl7tCUFeTiX/C74B0m7WUpXDTrQAAAAASUVORK5CYII=";
    document.querySelector('.healthy div img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAF3SURBVHgBxZS9S8NQEMDvXlKKH3EpKO1SkKJLO1inKm6KCOIgKG7ODk4ddBE6OFj/CzexW9VFi7h0VJGCYBdFQafi0Bow8b3zpVBMpM1HLfQH4eUdd78cL5cA9Bj0SljaToRHovE4Ijf0D7NWPCzXuxJu5BdixDFNABFCCLfi9M2qKufV49zlm6fQ6mY4Np5EgqRd0r6Q6kTKMxgvN4Xcg9GKM3vS0GgiI5dpL1mzU0ANUKQgFM/Y4w4hqmICgsL4ZEdhLwgkJBIzRLAn18F/Cy2ZTN+Ub+MOkendCQm2mmPzKzuVY3HmVqK6+hB0Kc1KWcSPzFMoBUeyw3V5o/uReQqbUoQTCIDjDAWHdwjI3xqH0Ax9XgtgdfAJylyrxh5T7JvH0quRnJuqgmIqgDTmalN5pTHwdHWevdedD+nA2sF8Ss5bmoTzu5Y/ji9i4rawW6pA265dWMnPaiGhLTMQmrUXgDWTNS6KO2Xfx9KW1f3FqHVBP/gBMv6AZ7+SMuoAAAAASUVORK5CYII=";
    document.querySelector('.downtime-analysis div img').src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAYAAADEtGw7AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAF/SURBVHgB1ZS7SkNBEIb/2dyjASGFIGoVEhsxIAhewNZCwcbUvoOtdrY2Nr6FCOITiGUgEAkklSBoFRGiuWH2d08uJod4kpOLhV81uzv7DbMsA/wR8tvmZSwWWPbM+00YwWBKh/mH0lDxTWI74gvKLoEFuEWhROinSu09ncrl6t3tHqk3LPsjSS00IqLVajAwd2Cv18YTknUrCWOitIrere3E+8RCxDEhmkjYxNcrG1FMAfFg1iYOzwT8mDLK+Ug2SUn9LMkwSdedOYpJFAgmjfy4VUcdQcmJW7mjWIRFgVwYeZyQM/MNt0z6c0feAJasLkYW2+RkSMBbob4SSt6SK8ipSVh0uuvFEJpy8txE5fZWAZTksHtDxW17uRvrikBlrJDaFBVxFpc/a3W/2x9HMVJmWvXEMa35xoq+OqaAbmj7EGqOPrKGCVHCD5u4iVceMSFfVZXtE5erb1lNXcS4aKRN5y+dZd/rW6PPmlK9A2WgT+ui3+vJ7qXvX/Gv+QYCZI10cYyt4QAAAABJRU5ErkJggg==";
